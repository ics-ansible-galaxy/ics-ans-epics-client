import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('epics_client')


def test_pvvalidator(host):
    cmd = host.run("source /etc/profile && pvValidator.py --version")
    assert cmd.succeeded
